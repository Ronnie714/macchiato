using Steamworks;
using System;
using System.Collections.Generic;
using System.IO;
using Unity.Netcode;
using UnityEngine;

namespace Mirror.FizzySteam
{
    [RequireComponent(typeof(SteamManager))]
    [HelpURL("https://github.com/Chykary/FizzySteamworks")]
    public class FizzySteamworks : UnityTransport
    {
        private const string STEAM_SCHEME = "steam";

        private Client client;
        private Server server;

        private Common activeNode;

        [SerializeField]
        public EP2PSend[] Channels = new EP2PSend[1] { EP2PSend.k_EP2PSendReliable };

        [Tooltip("Timeout for connecting in seconds.")]
        public int Timeout = 25;
        [Tooltip("The Steam ID for your application.")]
        public string SteamAppID = "480";
        [Tooltip("Allow or disallow P2P connections to fall back to being relayed through the Steam servers if a direct connection or NAT-traversal cannot be established.")]
        public bool AllowSteamRelay = true;

        [Header("Info")]
        [Tooltip("This will display your Steam User ID when you start or connect to a server.")]
        public ulong SteamUserID;

        private void Awake()
        {
            const string fileName = "steam_appid.txt";
            if (File.Exists(fileName))
            {
                string content = File.ReadAllText(fileName);
                if (content != SteamAppID)
                {
                    File.WriteAllText(fileName, SteamAppID.ToString());
                    Debug.Log($"Updating {fileName}. Previous: {content}, new SteamAppID {SteamAppID}");
                }
            }
            else
            {
                File.WriteAllText(fileName, SteamAppID.ToString());
                Debug.Log($"New {fileName} written with SteamAppID {SteamAppID}");
            }

            Debug.Assert(Channels != null && Channels.Length > 0, "No channel configured for FizzySteamworks.");

            Invoke(nameof(FetchSteamID), 1f);
        }

        private void LateUpdate()
        {
            if (enabled)
            {
                activeNode?.ReceiveData();
            }
        }

        public bool ClientConnected() => ClientActive() && client.Connected;
        public void ClientConnect(string address)
        {
            if (!SteamManager.Initialized)
            {
                Debug.LogError("SteamWorks not initialized. Client could not be started.");
                OnClientDisconnected.Invoke();
                return;
            }

            FetchSteamID();

            if (ServerActive())
            {
                Debug.LogError("Transport already running as server!");
                return;
            }

            if (!ClientActive() || client.Error)
            {
                Debug.Log($"Starting client, target address {address}.");

                SteamNetworking.AllowP2PPacketRelay(AllowSteamRelay);
                client = Client.CreateClient(this, address);
                activeNode = client;
            }
            else
            {
                Debug.LogError("Client already running!");
            }
        }

        public void ClientConnect(Uri uri)
        {
            if (uri.Scheme != STEAM_SCHEME)
                throw new ArgumentException($"Invalid url {uri}, use {STEAM_SCHEME}://SteamID instead", nameof(uri));

            ClientConnect(uri.Host);
        }

        public void ClientSend(int channelId, ArraySegment<byte> segment)
        {
            byte[] data = new byte[segment.Count];
            Array.Copy(segment.Array, segment.Offset, data, 0, segment.Count);
            client.Send(data, channelId);
        }

        public void ClientDisconnect()
        {
            if (ClientActive())
            {
                Shutdown();
            }
        }
        public bool ClientActive() => client != null;


        public bool ServerActive() => server != null;
        public void ServerStart()
        {
            if (!SteamManager.Initialized)
            {
                Debug.LogError("SteamWorks not initialized. Server could not be started.");
                return;
            }

            FetchSteamID();

            if (ClientActive())
            {
                Debug.LogError("Transport already running as client!");
                return;
            }

            if (!ServerActive())
            {
                Debug.Log("Starting server.");
                SteamNetworking.AllowP2PPacketRelay(AllowSteamRelay);
                server = Server.CreateServer(this, 4);
                activeNode = server;
            }
            else
            {
                Debug.LogError("Server already started!");
            }
        }

        public Uri ServerUri()
        {
            var steamBuilder = new UriBuilder
            {
                Scheme = STEAM_SCHEME,
                Host = SteamUser.GetSteamID().m_SteamID.ToString()
            };

            return steamBuilder.Uri;
        }

        public void ServerSend(int connectionId, int channelId, ArraySegment<byte> segment)
        {
            if (ServerActive())
            {
                byte[] data = new byte[segment.Count];
                Array.Copy(segment.Array, segment.Offset, data, 0, segment.Count);
                server.SendAll(connectionId, data, channelId);
            }
        }
        public bool ServerDisconnect(int connectionId) => ServerActive() && server.Disconnect(connectionId);
        public string ServerGetClientAddress(int connectionId) => ServerActive() ? server.ServerGetClientAddress(connectionId) : string.Empty;
        public void ServerStop()
        {
            if (ServerActive())
            {
                Shutdown();
            }
        }

        public override void Shutdown()
        {
            server?.Shutdown();
            client?.Disconnect();

            server = null;
            client = null;
            activeNode = null;
            Debug.Log("Transport shut down.");
        }

        public int GetMaxPacketSize(int channelId)
        {
            switch (Channels[channelId])
            {
                case EP2PSend.k_EP2PSendUnreliable:
                case EP2PSend.k_EP2PSendUnreliableNoDelay:
                    return 1200;
                case EP2PSend.k_EP2PSendReliable:
                case EP2PSend.k_EP2PSendReliableWithBuffering:
                    return 1048576;
                default:
                    throw new NotSupportedException();
            }
        }

        public bool Available()
        {
            try
            {
                return SteamManager.Initialized;
            }
            catch
            {
                return false;
            }
        }

        private void FetchSteamID()
        {
            if (SteamManager.Initialized)
            {
                SteamUserID = SteamUser.GetSteamID().m_SteamID;
            }
        }

        private void OnDestroy()
        {
            if (activeNode != null)
            {
                Shutdown();
            }
        }
        //Custom ===================================================================================

        struct InternalEvent
        {
            public int clientId;
            public ArraySegment<byte> payload;
            public float receiveTime;
            public NetworkEvent evt;
        }

        private bool isServer = false;
        private bool isStarted = false;
        private Queue<InternalEvent> eventQueue = new Queue<InternalEvent>();
        public string address;


        public Action<ArraySegment<byte>, int> OnClientDataReceived;
        public Action OnClientConnected;
        public Action OnClientDisconnected;

        public Action<int> OnServerConnected;
        public Action<int> OnServerDisconnected;
        public Action<int, ArraySegment<byte>, int> OnServerDataReceived;
        public Action<int, Exception> OnServerError = (int id, Exception e) => Debug.LogWarning("OnServerError called with no handler");


        public override void Send(ulong clientId, ArraySegment<byte> payload, NetworkDelivery networkDelivery)
        {
            if (isServer)
            {
                ServerSend((int)clientId, 0, payload);
            }
            else
            {
                ClientSend(0, payload);
            }
        }

        public override NetworkEvent PollEvent(out ulong clientId, out ArraySegment<byte> payload, out float receiveTime)
        {
            clientId = default;
            payload = default;
            receiveTime = default;
            return NetworkEvent.Nothing;
        }

        public override bool StartClient()
        {
            ClientConnect(address);
            isStarted = true;
            return true;
        }

        public override bool StartServer()
        {
            isServer = true;
            ServerStart();
            isStarted = true;
            address = "" + ServerClientId;
            return true;
        }

        public override void DisconnectRemoteClient(ulong clientId)
        {
            ServerDisconnect((int)clientId);
        }

        public override void DisconnectLocalClient()
        {
            ClientDisconnect();
        }

        public override ulong GetCurrentRtt(ulong clientId)
        {
            //throw new NotImplementedException();
            Debug.LogWarning("RTT requested... BS returned");
            return 50;
        }

        public override void Initialize()
        {
            OnClientDataReceived = (ArraySegment<byte> payload, int channel) =>
            {
                Debug.LogWarning("OnClientDataReceived called with no handler");
                InternalEvent e;
                e.clientId = default;
                e.payload = payload;
                e.receiveTime = Time.realtimeSinceStartup;
                e.evt = NetworkEvent.Data;
                eventQueue.Enqueue(e);
            };
            OnClientConnected = () =>
            {
                Debug.LogWarning("OnClientConnected called with no handler");
                InternalEvent e;
                e.clientId = default;
                e.payload = default;
                e.receiveTime = Time.realtimeSinceStartup;
                e.evt = NetworkEvent.Connect;
                eventQueue.Enqueue(e);
            };
            OnClientDisconnected = () => {
                Debug.LogWarning("OnClientDisconnected called with no handler");
                InternalEvent e;
                e.clientId = default;
                e.payload = default;
                e.receiveTime = Time.realtimeSinceStartup;
                e.evt = NetworkEvent.Disconnect;
                eventQueue.Enqueue(e);
            };

            OnServerDataReceived = (int id, ArraySegment<byte> payload, int channel) =>
            {
                Debug.LogWarning("OnServerDataReceived called with no handler");
                InternalEvent e;
                e.clientId = id;
                e.payload = payload;
                e.receiveTime = Time.realtimeSinceStartup;
                e.evt = NetworkEvent.Data;
                eventQueue.Enqueue(e);
            };
            OnServerConnected = (int id) =>
            {
                Debug.LogWarning("OnServerConnected called with no handler");
                InternalEvent e;
                e.clientId = id;
                e.payload = default;
                e.receiveTime = Time.realtimeSinceStartup;
                e.evt = NetworkEvent.Connect;
                eventQueue.Enqueue(e);
            };
            OnServerDisconnected = (int id) =>
            {
                Debug.LogWarning("OnServerDisconnected called with no handler");
                InternalEvent e;
                e.clientId = id;
                e.payload = default;
                e.receiveTime = Time.realtimeSinceStartup;
                e.evt = NetworkEvent.Disconnect;
                eventQueue.Enqueue(e);
            };

            Steamworks.SteamAPI.Init();
        }

        public void Update()
        {
            if (isStarted)
            {
                if (eventQueue.Count > 0)
                {

                    InternalEvent e = eventQueue.Dequeue();

                    Debug.Log(e.clientId);
                    Debug.Log((ulong)e.clientId);
                    Debug.Log((int)((ulong)e.clientId));

                    InvokeOnTransportEvent(e.evt,
                        (ulong)e.clientId,
                        e.payload,
                        e.receiveTime);

                }
            }
        }
    }
}