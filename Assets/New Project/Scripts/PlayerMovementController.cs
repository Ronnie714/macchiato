﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementController : MonoBehaviour
{
    PlayerInputController pic;
    Rigidbody rb;
    float speed;
    private bool boosted;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        pic = GetComponent<PlayerInputController>();
        speed = 70;
    }


    void FixedUpdate()
    {
        float boost = 1;
        
        if (pic.Right) {
            rb.AddForce(new Vector3(1, 0, 0) * speed * boost, ForceMode.Force);
        }
        
        if (pic.Left) {
            rb.AddForce(new Vector3(-1, 0, 0) * speed * boost, ForceMode.Force);
        }
        
        if (pic.Up) {
            rb.AddForce(new Vector3(0, 0, 1) * speed * boost, ForceMode.Force);
        }

        if (pic.Down) {
            rb.AddForce(new Vector3(0, 0, -1) * speed * boost, ForceMode.Force);
        }

        Debug.Log("HI: " + pic.Shift);
        if (pic.Shift && !boosted) {
            boosted = true;
            rb.AddForce(rb.velocity.normalized * 50, ForceMode.Impulse);
        }

        if (!pic.Shift) { boosted = false; }
    }
}
