﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class Manager : NetworkBehaviour
{

    public GameObject PrefabBank, PlayersFolder, PlayersInfoFolder, SpawnPointsFolder;
    public List<GameObject> players { get; private set; }
    // Start is called before the first frame update
    void Start()
    {
        players = new List<GameObject>();
        foreach (Transform t in PlayersInfoFolder.transform)
        {
            Debug.Log(t.gameObject);
            spawnPlayer(t.GetComponent<PlayerInformation>());
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void spawnPlayer(PlayerInformation pi)
    {
        if (IsServer)
        {
            GameObject p = Instantiate(PrefabBank.GetComponent<PrefabBank>().playerTypes[pi.TypeID]);
            //p.GetComponent<PlayerInputController>().info = pi;
            p.transform.position = SpawnPointsFolder.transform.GetChild(pi.PlayerID).transform.position;
            players.Add(p);
            p.GetComponent<NetworkObject>().Spawn(true);
            p.transform.parent = PlayersFolder.transform;
            p.GetComponent<PlayerInputController>().setInfo(pi);
        }

    }
}
