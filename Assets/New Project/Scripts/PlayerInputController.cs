﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Unity.Netcode;

public class PlayerInputController : NetworkBehaviour
{
    public PlayerInformation info;
    public bool Right { get; private set; }
    public bool Left { get; private set; }
    public bool Up { get; private set; }
    public bool Down { get; private set; }
    public bool Shift { get; private set; }

    /*bool _shiftDown;
    public bool shiftDown
    {
        get
        {
            return _shiftDown && (Time.frameCount == sFrame + 1);
        }

        set
        {
            if (_shiftDown != value)
            {
                _shiftDown = value;
                sFrame = Time.frameCount;
            }
        }
    }
    private int sFrame = 0;*/

    public struct DataPoint
    {
        public bool Right;
        public bool Left;
        public bool Up;
        public bool Down;
        public bool Shift;
    }


    void Start()
    {
        
    }


    [ServerRpc]
    public void updateServerControlsServerRpc(DataPoint point)
    {
        Right = point.Right;
        Left = point.Left;
        Up = point.Up;
        Down = point.Down;
        Shift = point.Shift;
        Debug.Log("TEST: " + point.Right + " " + point.Left);
    }

    void Update()
    {
        if (info != null)
        {
            if (IsServer && info.PlayerID == 0)
            {
                Right = Input.GetKey("d");
                Left = Input.GetKey("a");
                Up = Input.GetKey("w");
                Down = Input.GetKey("s");
                Shift = Input.GetKey(KeyCode.Space);
            }
            if (IsClient && !IsServer && info.PlayerID == 1)
            {
                DataPoint point;
                point.Right = Input.GetKey("d");
                point.Left = Input.GetKey("a");
                point.Up = Input.GetKey("w");
                point.Down = Input.GetKey("s");
                point.Shift = Input.GetKey(KeyCode.Space);
                updateServerControlsServerRpc(point);
            }


        }
        //shiftDown = Shift;
    }

    public void setInfo(PlayerInformation pi)
    {
        if (IsServer)
        {
            this.info = pi;
            if(pi.PlayerID == 1)
            {
                gameObject.GetComponent<NetworkObject>().ChangeOwnership(1);
            }
            setInfoClientRpc(pi.PlayerID, pi.TypeID);
        }
    }

    [ClientRpc]
    private void setInfoClientRpc(int pid, int tid)
    {
        GameObject go = new GameObject();
        go.AddComponent<PlayerInformation>();
        this.info = go.GetComponent<PlayerInformation>();
        this.info.PlayerID = pid;
        this.info.TypeID = tid;
    }
}
